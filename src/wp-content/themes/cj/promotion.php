<?php
/**
 *Template Name: Promotion
 */
get_header();
?>
<!-- main -->
   
 <!-- main -->
   
 <section id="promotion">
   <ul class="promotion-list">
   <?php 
		$post_type = 'promotion';
		$wp_query_args = array();
		$query_args = array(
            'post_type' => $post_type,
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'ignore_sticky_posts' => true
            );
		$query_args = wp_parse_args( $wp_query_args, $query_args );
	      $my_query = new WP_Query( $query_args );
		if( $my_query->have_posts()) { ?>
      <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
      <li>
         <a target="_blank"  href="<?php echo get_post_meta( $post->ID, 'wpcf-page-promotion-link', true ); ?>">
            <img class="img-mb" src="<?php echo get_post_meta( $post->ID, 'wpcf-page-promotion-hinh-khuyen-mai-mb', true ); ?>" alt="">
            <img class="img-desk" src="<?php echo get_post_meta( $post->ID, 'wpcf-page-promotion-hinh-khuyen-mai', true ); ?>" alt="">
         </a>
      </li>
      <?php endwhile; ?>
      <?php
		}
		?>
   </ul>
   <div class="ft">
      <img class="img-mb" src="<?php bloginfo('template_directory');?>/images/m-dealer-footer.png" alt="">
      <img class="img-desk" src="<?php bloginfo('template_directory');?>/images/dealer-footer.png" alt="">
   </div>
</section>

   <!-- end main -->


	<script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/promotion.js"></script>
<?php
get_footer();
