<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
   <!-- Locale -->
   <meta http-equiv="Content-Language" content="en">
   <!-- To the Future! -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
   <!-- CSS -->
   <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/styles.css" />

   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/jquery.min.js"></script>
   <?php the_field('google_analytic', 'option'); ?>
   <?php wp_head();?>
</head>
<body>
<?php 
   $current_page = 'home';
   if($wp->request){
   $current_page =$wp->request;
   }
?>
   <header id="header">
      <div class="nav-wrap">
         <a class="logo" href="./">
            <img src="<?php bloginfo('template_directory');?>/images/logo.png" alt="">
         </a>
         <a class="nav-control" href="javascript:;">
            <span></span>
         </a>
         <ul class="main-nav">
            <li>
               <a class="<?php if($current_page == 'home') echo 'active';?>" href="<?php home_url();?>/" data-href="#banner">TRANG CHỦ</a>
            </li>
            <li>
               <a href="<?php home_url();?>/#product" data-href="#product">SẢN PHẨM</a>
            </li>
            <li>
               <a href="<?php home_url();?>/#video" data-href="#video">TRUYỀN THÔNG</a>
            </li>
            <li>
               <a class="active" href="<?php home_url();?>/tam-la-vui">TẮM LÀ VUI</a>
            </li>
            <li>
               <a class="<?php if($current_page == 'khuyen-mai') echo 'active';?>" href="<?php home_url();?>/khuyen-mai">KHUYẾN MÃI</a>
            </li>
            <li>
               <a class="<?php if($current_page == 'phan-phoi') echo 'active';?>" href="<?php home_url();?>/phan-phoi">PHÂN PHỐI</a>
            </li>
            <li class="socials">
               <a href="<?php the_field('link_facebook', 'option'); ?>" target="_blank">
                  <i class="fa fa-facebook" aria-hidden="true"></i>
               </a>
               <a href="<?php the_field('link_youtube', 'option'); ?>" target="_blank">
                  <i class="fa fa-youtube-play" aria-hidden="true"></i>
               </a>
               <a href="<?php the_field('link_shop_online', 'option'); ?>" target="_blank">
                  <i class="fa fa-cart-plus" aria-hidden="true"></i>
               </a>
            </li>
         </ul>
      </div>
   </header>