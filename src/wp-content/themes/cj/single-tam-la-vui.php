<?php
get_header('pr-news');
?>
<div id="bubbles" class="pr-new-detail-bubble">
   <div class="dealer-bubble">
      <img class="bubble left-1" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble left-2" src="<?php bloginfo('template_directory');?>/images/bubble-cherry.png" alt="">
      <img class="bubble left-3" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble left-4" src="<?php bloginfo('template_directory');?>/images/bubble-berry.png" alt="">
      <img class="bubble left-5" src="<?php bloginfo('template_directory');?>/images/bubble-grapeberry.png" alt="">
      <img class="bubble left-6" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble right-2" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble right-3" src="<?php bloginfo('template_directory');?>/images/bubble-berry.png" alt="">
      <img class="bubble right-4" src="<?php bloginfo('template_directory');?>/images/bubble-grapeberry.png" alt="">
      <img class="bubble right-5" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble right-6" src="<?php bloginfo('template_directory');?>/images/bubble-cherry.png" alt="">
      <img class="bubble right-7" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
   </div>
</div>
<section id="pr_news">
    <div class="container">
        <?php while (have_posts()) : the_post();  ?>
            <div class="pr_news__content-wrapper">
                <h1><?php echo get_the_title();?></h1>
                <h2><?php echo get_the_excerpt();?></h2>
                <div class="pr_news__content-detail">
                    <?php echo the_content();?>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
    <div class="ft">
        <img class="img-mb" src="<?php bloginfo('template_directory');?>/images/m-dealer-footer.png" alt="">
        <img class="img-desk" src="<?php bloginfo('template_directory');?>/images/dealer-footer.png" alt="">
        <div class="footer-info">
            <div class="footer-info__wrapper container">
                <p class="footer-info__title">
                    Sữa tắm gội toàn thân Carrie Junior <br/>
                </p>
                <p class="footer-info__title-desc">Dưỡng chất Fruito-E giúp nuôi dưỡng làn da mỏng manh, nhạy cảm của bé yêu.</p>
                <p>Carrie Junior là thương hiệu dẫn đầu ngành hàng chăm sóc cá nhân dành cho trẻ em tại Malaysia hơn 20 năm qua. Với sứ mệnh trở thành thương hiệu được "Bố mẹ tin dùng, bé yêu vui thích", Carrie Junior đã ra mắt dòng sản phẩm tắm gội cho trẻ từ 2 tuổi tại Việt Nam, hứa hẹn sẽ mang đến cho bé những trải nghiệm vui nhộn và thú vị trong khoảng thời gian tắm.</p>
                <p>Sữa tắm gội cho bé từ 2 tuổi Carrie Junior hiện đang có mặt tại các hệ thống siêu thị như Guardian, Aeon Wellness, BigC, Vinmart, Lotte, Coop Mart, Emart... các cửa hàng bán lẻ uy tín tại các thành phố như TP Hồ Chí Minh, Hà Nội, Cần Thơ... và thông qua kênh bán hàng online như Lazada, Shopee.</p>
                <p>Website: <a href="https://carriejunior.com.vn/">https://carriejunior.com.vn/</a></p>
                <p>Facebook: <a href="https://www.facebook.com/CarrieJuniorVietnam/">https://www.facebook.com/CarrieJuniorVietnam/</a></p>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/pr-news.js"></script>
<?php get_footer('pr-news-detail');
