<?php get_header();?>

<!-- main -->

<div id="bubbles">
   <div class="banner-bubble">
      <img class="bubble left-1" src="<?php bloginfo('template_directory');?>/images/bubble-grapeberry.png" alt="">
      <img class="bubble left-2" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble left-3" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble right-1" src="<?php bloginfo('template_directory');?>/images/bubble-berry.png" alt="">
      <img class="bubble right-2" src="<?php bloginfo('template_directory');?>/images/bubble-cherry.png" alt="">
      <img class="bubble right-3" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
   </div>
   <div class="product-bubble">
      <img class="bubble left-1" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble left-2" src="<?php bloginfo('template_directory');?>/images/bubble-cherry.png" alt="">
      <img class="bubble left-3" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble left-4" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble right-1" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble right-2" src="<?php bloginfo('template_directory');?>/images/bubble-berry.png" alt="">
      <img class="bubble right-3" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble right-4" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
   </div>
   <div class="video-bubble">
      <img class="bubble left-1" src="<?php bloginfo('template_directory');?>/images/bubble-grapeberry.png" alt="">
      <img class="bubble left-2" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble left-3" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble right-1" src="<?php bloginfo('template_directory');?>/images/bubble-cherry.png" alt="">
      <img class="bubble right-2" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble right-3" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
   </div>
</div>

<section id="banner">
   <div class="bg">
      <img class="img-mb" src="<?php bloginfo('template_directory');?>/images/m-banner-bg.jpg" alt="">
      <img class="img-desk" src="<?php bloginfo('template_directory');?>/images/banner-bg.jpg" alt="">
   </div>
   <div id="layer1" class="layer">
      <img class="img-mb" src="<?php bloginfo('template_directory');?>/images/m-banner-water.png" alt="">
      <img class="img-desk" src="<?php bloginfo('template_directory');?>/images/banner-water.png" alt="">
   </div>
   <div id="layer2" class="layer">
      <img class="img-mb" src="<?php bloginfo('template_directory');?>/images/m-banner-tree-1.png" alt="">
      <img class="img-desk" src="<?php bloginfo('template_directory');?>/images/banner-tree-1.png" alt="">
   </div>
   <div id="layer3" class="layer">
      <img class="img-mb" src="<?php bloginfo('template_directory');?>/images/m-banner-title.png" alt="">
      <img class="img-desk" src="<?php bloginfo('template_directory');?>/images/banner-title.png" alt="">
   </div>
   <div id="layer4" class="layer">
      <img class="img-mb" src="<?php bloginfo('template_directory');?>/images/m-banner-character.png" alt="">
      <img class="img-desk" src="<?php bloginfo('template_directory');?>/images/banner-character.png" alt="">
   </div>
   <div id="layer5" class="layer">
      <img class="img-mb" src="<?php bloginfo('template_directory');?>/images/m-banner-product.png" alt="">
      <img class="img-desk" src="<?php bloginfo('template_directory');?>/images/banner-product.png" alt="">
   </div>
   <div id="layer6" class="layer">
      <img class="img-mb" src="<?php bloginfo('template_directory');?>/images/m-banner-tree-2.png" alt="">
      <img class="img-desk" src="<?php bloginfo('template_directory');?>/images/banner-tree-2.png" alt="">
   </div>
   <div class="ft">
      <img class="img-mb" src="<?php bloginfo('template_directory');?>/images/m-banner-footer.png" alt="">
      <img class="img-desk" src="<?php bloginfo('template_directory');?>/images/banner-footer.png" alt="">
   </div>
</section>

<section id="product">
   <div id="list_tab" class="product-tab">
      <div class="fade-in-down">
         <h2 class="title">
            <span class="hidden-xs">
               <?php the_field('product_title', 'option'); ?>
            </span>
            <span class="hidden-sm hidden-md hidden-lg">
               <?php the_field('product_title_mb', 'option'); ?>
            </span>
         </h2>
      </div>
      <div class="fade-in-up">
         <div class="desc">
            <?php the_field('product_sub_desc', 'option'); ?>
         </div>
         <div class="content">
            <?php the_field('product_desc', 'option'); ?>
         </div>
      </div>
      <ul class="product-list">
         <li class="item" data-index="0">
            <div class="img-wrap fade-in-left">
               <img src="<?php bloginfo('template_directory');?>/images/m-product-cherry.png" alt="">
            </div>
            <div class="p-name name-cherry spin-360">
               <div class="icon">
                  <img src="<?php bloginfo('template_directory');?>/images/icon-cherry.png" alt="">
               </div>
               <?php the_field('product_hc_title_product', 'option'); ?>
            </div>
            <div class="p-desc spin-360">
               <?php the_field('product_hc_sort_description', 'option'); ?>
            </div>
         </li>
         <li class="item" data-index="1">
            <div class="img-wrap blur-in">
               <img src="<?php bloginfo('template_directory');?>/images/m-product-grapeberry.png" alt="">
            </div>
            <div class="p-name name-grapeberry spin-360">
               <div class="icon">
                  <img src="<?php bloginfo('template_directory');?>/images/icon-grapeberry.png" alt="">
               </div>
               <?php the_field('product_hg_title_product', 'option'); ?>
            </div>
            <div class="p-desc spin-360">
               <?php the_field('product_hg_sort_description', 'option'); ?>
            </div>
         </li>
         <li class="item" data-index="2">
            <div class="img-wrap fade-in-right">
               <img src="<?php bloginfo('template_directory');?>/images/m-product-milk.png" alt="">
            </div>
            <div class="p-name name-milk spin-360">
               <div class="icon">
                  <img src="<?php bloginfo('template_directory');?>/images/icon-milk.png" alt="">
               </div>
               <?php the_field('product_tcs_title_product', 'option'); ?>
            </div>
            <div class="p-desc spin-360">
               <?php the_field('product_tcs_sort_description', 'option'); ?>
            </div>
         </li>
      </ul>
   </div>
   <div id="detail_tab" class="product-tab">
      <ul id="cat_list" class="cat-list">

         <!-- cherry detail list -->
         <li id="cherry_list" class="cat-item cherry-item">
            <div class="cat-header">
               <h2 class="title">
                  <span class="hidden-xs">
                     <?php the_field('product_title', 'option'); ?>
                  </span>
                  <span class="hidden-sm hidden-md hidden-lg">
                     <?php the_field('product_title_mb', 'option'); ?>
                  </span>
               </h2>
               <div class="detail-nav nav-mb">
                  <span>Dung tích:</span>
                  <ul class="nav-tab">
                     <li>
                        <a href="javascript:;" data-href="#cherry_700" class="active">700g</a>
                     </li>
                     <li>
                        <a href="javascript:;" data-href="#cherry_280">280g</a>
                     </li>
                  </ul>
                  <a href="<?php the_field('link_shop_online', 'option'); ?>" class="cart-btn" target="_blank">
                     <i class="fa fa-cart-plus" aria-hidden="true"></i>
                  </a>
               </div>
            </div>
            <ul class="detail-list">
               <li id="cherry_700" class="detail-item">
                  <div class="img-wrap">
                     <img src="<?php bloginfo('template_directory');?>/images/cherry-700.png" alt="">
                  </div>
                  <div class="content-wrap">
                     <div class="p-name">
                        <div class="icon">
                           <img src="<?php bloginfo('template_directory');?>/images/icon-cherry.png" alt="">
                        </div>
                        <?php the_field('product_hc_title_product', 'option'); ?>
                     </div>
                     <div class="p-content">
                        <?php the_field('product_hc_description', 'option'); ?>
                     </div>
                     <div class="p-offer">
                        <?php the_field('product_hc_khuyen_mai', 'option'); ?>
                     </div>
                  </div>
               </li>
               <li id="cherry_280" class="detail-item">
                  <div class="img-wrap">
                     <img src="<?php bloginfo('template_directory');?>/images/m-cherry-280.png" alt="">
                  </div>
                  <div class="content-wrap">
                     <div class="p-name">
                        <div class="icon">
                           <img src="<?php bloginfo('template_directory');?>/images/icon-cherry.png" alt="">
                        </div>
                        <?php the_field('product_hc_title_product', 'option'); ?>
                     </div>
                     <div class="p-content">
                        <?php the_field('product_hc_description', 'option'); ?>
                     </div>
                  </div>
               </li>
            </ul>
            <div class="detail-nav nav-desk">
               <span>Dung tích:</span>
               <ul class="nav-tab">
                  <li>
                     <a href="javascript:;" data-href="#cherry_700" class="active">700g</a>
                  </li>
                  <li>
                     <a href="javascript:;" data-href="#cherry_280">280g</a>
                  </li>
               </ul>
               <a href="<?php the_field('link_shop_online', 'option'); ?>" class="cart-btn" target="_blank">
                  <i class="fa fa-cart-plus" aria-hidden="true"></i>
               </a>
            </div>
         </li>

         <!-- grapeberry detail list -->
         <li id="grapeberry_list" class="cat-item grapeberry-item">
            <div class="cat-header">
               <h2 class="title">
                  <span class="hidden-xs">
                     <?php the_field('product_title', 'option'); ?>
                  </span>
                  <span class="hidden-sm hidden-md hidden-lg">
                     <?php the_field('product_title_mb', 'option'); ?>
                  </span>
               </h2>
               <div class="detail-nav nav-mb">
                  <span>Dung tích:</span>
                  <ul class="nav-tab">
                     <li>
                        <a href="javascript:;" data-href="#grapeberry_700" class="active">700g</a>
                     </li>
                     <li>
                        <a href="javascript:;" data-href="#grapeberry_280">280g</a>
                     </li>
                  </ul>
                  <a href="<?php the_field('link_shop_online', 'option'); ?>" class="cart-btn" target="_blank">
                     <i class="fa fa-cart-plus" aria-hidden="true"></i>
                  </a>
               </div>
            </div>
            <ul class="detail-list">
               <li id="grapeberry_700" class="detail-item">
                  <div class="img-wrap">
                     <img src="<?php bloginfo('template_directory');?>/images/grapeberry-700.png" alt="">
                  </div>
                  <div class="content-wrap">
                     <div class="p-name">
                        <div class="icon">
                           <img src="<?php bloginfo('template_directory');?>/images/icon-grapeberry.png" alt="">
                        </div>
                        <?php the_field('product_hg_title_product', 'option'); ?>
                     </div>
                     <div class="p-content">
                        <?php the_field('product_hg_description', 'option'); ?>
                     </div>
                     <div class="p-offer">
                        <?php the_field('product_hg_khuyen_mai', 'option'); ?>
                     </div>
                  </div>
               </li>
               <li id="grapeberry_280" class="detail-item">
                  <div class="img-wrap">
                     <img src="<?php bloginfo('template_directory');?>/images/m-grapeberry-280.png" alt="">
                  </div>
                  <div class="content-wrap">
                     <div class="p-name">
                        <div class="icon">
                           <img src="<?php bloginfo('template_directory');?>/images/icon-grapeberry.png" alt="">
                        </div>
                        <?php the_field('product_hg_title_product', 'option'); ?>
                     </div>
                     <div class="p-content">
                        <?php the_field('product_hg_description', 'option'); ?>
                     </div>
                  </div>
               </li>
            </ul>
            <div class="detail-nav nav-desk">
               <span>Dung tích:</span>
               <ul class="nav-tab">
                  <li>
                     <a href="javascript:;" data-href="#grapeberry_700" class="active">700g</a>
                  </li>
                  <li>
                     <a href="javascript:;" data-href="#grapeberry_280">280g</a>
                  </li>
               </ul>
               <a href="<?php the_field('link_shop_online', 'option'); ?>" class="cart-btn" target="_blank">
                  <i class="fa fa-cart-plus" aria-hidden="true"></i>
               </a>
            </div>
         </li>
         <!-- milk detail list -->
         <li id="milk_list" class="cat-item milk-item">
            <div class="cat-header">
               <h2 class="title">
                  <span class="hidden-xs">
                     <?php the_field('product_title', 'option'); ?>
                  </span>
                  <span class="hidden-sm hidden-md hidden-lg">
                     <?php the_field('product_title_mb', 'option'); ?>
                  </span>
               </h2>
               <div class="detail-nav nav-mb">
                  <span>Dung tích:</span>
                  <ul class="nav-tab">
                     <li>
                        <a href="javascript:;" data-href="#milk_700" class="active">700g</a>
                     </li>
                     <li>
                        <a href="javascript:;" data-href="#milk_280">280g</a>
                     </li>
                  </ul>
                  <a href="<?php the_field('link_shop_online', 'option'); ?>" class="cart-btn" target="_blank">
                     <i class="fa fa-cart-plus" aria-hidden="true"></i>
                  </a>
               </div>
            </div>
            <ul class="detail-list">
               <li id="milk_700" class="detail-item">
                  <div class="img-wrap">
                     <img src="<?php bloginfo('template_directory');?>/images/milk-700.png" alt="">
                  </div>
                  <div class="content-wrap">
                     <div class="p-name">
                        <div class="icon">
                           <img src="<?php bloginfo('template_directory');?>/images/icon-milk.png" alt="">
                        </div>
                        <?php the_field('product_tcs_title_product', 'option'); ?>
                     </div>
                     <div class="p-content">
                        <?php the_field('product_tcs_description', 'option'); ?>
                     </div>
                     <div class="p-offer">
                        <?php the_field('product_tcs_khuyen_mai', 'option'); ?>
                     </div>
                  </div>
               </li>
               <li id="milk_280" class="detail-item">
                  <div class="img-wrap">
                     <img src="<?php bloginfo('template_directory');?>/images/m-milk-280.png" alt="">
                  </div>
                  <div class="content-wrap">
                     <div class="p-name">
                        <div class="icon">
                           <img src="<?php bloginfo('template_directory');?>/images/icon-milk.png" alt="">
                        </div>
                        <?php the_field('product_tcs_title_product', 'option'); ?>
                     </div>
                     <div class="p-content">
                        <?php the_field('product_tcs_description', 'option'); ?>
                     </div>
                  </div>
               </li>
            </ul>
            <div class="detail-nav nav-desk">
               <span>Dung tích:</span>
               <ul class="nav-tab">
                  <li>
                     <a href="javascript:;" data-href="#milk_700" class="active">700g</a>
                  </li>
                  <li>
                     <a href="javascript:;" data-href="#milk_280">280g</a>
                  </li>
               </ul>
               <a href="<?php the_field('link_shop_online', 'option'); ?>" class="cart-btn" target="_blank">
                  <i class="fa fa-cart-plus" aria-hidden="true"></i>
               </a>
            </div>
         </li>
      </ul>
   </div>

   <div class="ft">
      <img class="img-mb" src="<?php bloginfo('template_directory');?>/images/m-product-footer.png" alt="">
      <img class="img-desk" src="<?php bloginfo('template_directory');?>/images/product-footer.png" alt="">
   </div>
</section>

<section id="video">
   <h2 class="title txt-decoration">TRUYỀN THÔNG</h2>
   <ul class="vid-list vid-scale">
   <?php
      $list_youtube = get_field('video_truyen_thong', 'option');
      $arr = explode(";", $list_youtube);
      var_dump($arr);
   ?>
   <?php foreach ($arr as $key => $value) {
   ?>
      <li>
         <a class="vid-item" href="javascript:;" data-url="<?php echo $value;?>">
            <img src="https://img.youtube.com/vi/<?php echo  $value;?>/maxresdefault.jpg" alt="">
         </a>
      </li>

   <?php } ?>
      <!--
      <li>
         <a class="vid-item" href="javascript:;" data-url="RxycM9tG6yU">
            <img src="<?php bloginfo('template_directory');?>/images/vid-thumb-2.jpg" alt="">
         </a>
      </li> -->
   </ul>
</section>

   <!-- end main -->
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/home.js"></script>
<?php get_footer();?>