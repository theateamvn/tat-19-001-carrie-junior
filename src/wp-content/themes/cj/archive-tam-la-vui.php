<?php
    get_header('pr-news');
?>
<!-- main -->
<div id="bubbles">
   <div class="dealer-bubble">
      <img class="bubble left-1" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble left-2" src="<?php bloginfo('template_directory');?>/images/bubble-cherry.png" alt="">
      <img class="bubble left-3" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble left-4" src="<?php bloginfo('template_directory');?>/images/bubble-berry.png" alt="">
      <img class="bubble left-5" src="<?php bloginfo('template_directory');?>/images/bubble-grapeberry.png" alt="">
      <img class="bubble left-6" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble right-2" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble right-3" src="<?php bloginfo('template_directory');?>/images/bubble-berry.png" alt="">
      <img class="bubble right-4" src="<?php bloginfo('template_directory');?>/images/bubble-grapeberry.png" alt="">
      <img class="bubble right-5" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
      <img class="bubble right-6" src="<?php bloginfo('template_directory');?>/images/bubble-cherry.png" alt="">
      <img class="bubble right-7" src="<?php bloginfo('template_directory');?>/images/bubble-empty.png" alt="">
   </div>
</div>
<section id="pr_news" class="pr-news-list">
<div class="container">
    <div class="pr-new-wrapper">
        <ul class="pr-news-list list-inline">
            <?php 
            //$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $page= 1;
            $current_page=1;
            if(isset($_GET['trang'])){
                $page=$_GET['trang'];
                $current_page=$_GET['trang'];
            }
            query_posts(array(
                'post_type' => 'tam-la-vui', // You can add a custom post type if you like
                'post_status'=>'publish',
                'paged' => $page,
                'posts_per_page' => 8 // limit of posts
            ));
            while (have_posts()) : the_post();  ?>
                <li class="li-wrapper">
                    <div class="pr-news-list-thumb">
                        <a class="view-detail-wrapper" href="<?php echo get_permalink();?>">
                            <img src="<?php echo the_post_thumbnail_url('thumbnail'); ?>" alt="">
                        </a>
                    </div>
                    <div class="pr-news-list-content-wrapper">
                    <a href="<?php echo get_permalink();?>"><p><?php echo get_the_title();?></p>
                        << xem thêm</a>
                    </div>
                </li>
            <?php endwhile;?>
            <?php post_pagination($current_page); ?>
        </ul>
    </div>
    <div class="ft">
        <img class="img-mb" src="<?php bloginfo('template_directory');?>/images/m-dealer-footer.png" alt="">
        <img class="img-desk" src="<?php bloginfo('template_directory');?>/images/dealer-footer.png" alt="">
    </div>
</div>
</section>
<!-- end main -->
<?php
function post_pagination($current_page){
    the_posts_pagination( array(
        'screen_reader_text' => __( ' ' ),
        'base'      => home_url().'/tam-la-vui%_%',
        'format'    => '?trang=%#%',
        'current'   => $current_page,
        'mid_size'  => 8,
        'before_page_number' => '',
        'prev_text' => __( '&lt;', 'basic'),
        'next_text' => __( '&gt;', 'basic'),
    ));
}
?>
<script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/pr-news.js"></script>
<?php get_footer('pr-news');