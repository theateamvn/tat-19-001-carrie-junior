$(function () {
   var enableCatSlide = true;

   $('#video .vid-list').slick({
      prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
      nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
   });

   // init the controller
   var controller = new ScrollMagic.Controller();
   var winWidth = window.innerWidth;
   var winHeight = window.innerHeight;
   var bannerHeight = $('#banner').height();
   var productOffset = $('#product').offset().top - winHeight/2;
   var productHeight = $('#product').height();
   var videoOffset = $('#video').offset().top - winHeight/2;
   var videoHeight = $('#video').height();
   var scroll = $(window).scrollTop();
   
   // Scrolling parallax animation settings
   
   // Banner
   var tlBubbleBanner = new TimelineMax({ yoyo: true });
   tlBubbleBanner.add([
      TweenMax.to(".banner-bubble .left-1", 3, {
         yPercent: -60,
         ease: Power2.easeOut
      }),
      TweenMax.to(".banner-bubble .left-2", 3, {
         yPercent: -240,
         ease: Power2.easeOut
      }),
      TweenMax.to(".banner-bubble .left-3", 3, {
         yPercent: -480,
         ease: Power2.easeOut
      }),
      TweenMax.to(".banner-bubble .right-1", 3, {
         yPercent: -40,
         ease: Power2.easeOut
      }),
      TweenMax.to(".banner-bubble .right-2", 3, {
         yPercent: -150,
         ease: Power2.easeOut
      }),
      TweenMax.to(".banner-bubble .right-3", 3, {
         yPercent: -180,
         ease: Power2.easeOut
      })
   ]);
    
   var tlBubbleProduct = new TimelineMax({ yoyo: true });
   tlBubbleProduct.add([
      TweenMax.to(".product-bubble .left-1", 3, {
         yPercent: -1000,
         ease: Power2.easeOut
      }),
      TweenMax.to(".product-bubble .left-2", 3, {
         yPercent: -950,
         ease: Power2.easeOut
      }),
      TweenMax.to(".product-bubble .left-3", 3, {
         yPercent: -1000,
         ease: Power2.easeOut
      }),
      TweenMax.to(".product-bubble .left-4", 3, {
         yPercent: -1300,
         ease: Power2.easeOut
      }),
      TweenMax.to(".product-bubble .right-1", 3, {
         yPercent: -580,
         ease: Power2.easeOut
      }),
      TweenMax.to(".product-bubble .right-2", 3, {
         yPercent: -1000,
         ease: Power2.easeOut
      }),
      TweenMax.to(".product-bubble .right-3", 3, {
         yPercent: -1500,
         ease: Power2.easeOut
      }),
      TweenMax.to(".product-bubble .right-4", 3, {
         yPercent: -650,
         ease: Power2.easeOut
      })
   ]);
    
   var tlBubbleVideo = new TimelineMax({ yoyo: true });
   tlBubbleVideo.add([
      TweenMax.to(".video-bubble .left-1", 3, {
         yPercent: -230,
         ease: Power2.easeOut
      }),
      TweenMax.to(".video-bubble .left-2", 3, {
         yPercent: -500,
         ease: Power2.easeOut
      }),
      TweenMax.to(".video-bubble .left-3", 3, {
         yPercent: -1000,
         ease: Power2.easeOut
      }),
      TweenMax.to(".video-bubble .right-1", 3, {
         yPercent: -280,
         ease: Power2.easeOut
      }),
      TweenMax.to(".video-bubble .right-2", 3, {
         yPercent: -1200,
         ease: Power2.easeOut
      }),
      TweenMax.to(".video-bubble .right-3", 3, {
         yPercent: -1000,
         ease: Power2.easeOut
      })
   ]);

   // Start pin & animation when scrolling
   var sceneBanner = new ScrollMagic.Scene({
      triggerElement: "#banner",
      duration: bannerHeight * 1.5,
      triggerHook: "onLeave"
   })
   .setTween(tlBubbleBanner)
   .addTo(controller);

   var sceneProduct = new ScrollMagic.Scene({
      triggerElement: "#product",
      duration: productHeight * 1.5
   })
   .setTween(tlBubbleProduct)
   .addTo(controller);

   var sceneVideo = new ScrollMagic.Scene({
      triggerElement: "#video",
      duration: videoHeight * 1.5,
      offset: -winHeight/3
   })
   .setTween(tlBubbleVideo)
   .addTo(controller);

   // End scrolling parallax animation settings

   $('.main-nav a').on('click', function(e) {
      var href = $(this).data('href');
      if (href) {
         e.stopPropagation();
         e.preventDefault();
         $('body').removeClass('nav-active');
         if (href == '#product') {
            $('#detail_tab').hide();
            $('#list_tab').fadeIn(300);
         }
         if ($(href).length > 0) {
            if (winWidth < 768) {
               $('body, html').stop().animate({
                  scrollTop: $(href).offset().top - 60
               }, 700, 'swing');
            }
            else if (winWidth >= 1600) {
               $('body, html').stop().animate({
                  scrollTop: $(href).offset().top - 85
               }, 700, 'swing');
            }
            else {
               $('body, html').stop().animate({
                  scrollTop: $(href).offset().top - 45
               }, 700, 'swing');
            }
         }
      }
   });

   function checkActiveSection() {
      if (scroll >= videoOffset) {
         $('.main-nav a').removeClass('active');
         $('.main-nav a[data-href="#video"]').addClass('active');
      }
      else if (scroll >= productOffset) {
         $('.main-nav a').removeClass('active');
         $('.main-nav a[data-href="#product"]').addClass('active');
      }
      else {
         $('.main-nav a').removeClass('active');
         $('.main-nav a[data-href="#banner"]').addClass('active');
      }
   }
   
   $(window).on('resize', function() {
      winWidth = window.innerWidth;
      winHeight = window.innerHeight;
      bannerHeight = $('#banner').height();
      productOffset = $('#product').offset().top - winHeight/2;
      productHeight = $('#product').height();
      videoOffset = $('#video').offset().top - winHeight/2;
      videoHeight = $('#video').height();
      scroll = $(window).scrollTop();
      checkActiveSection();

      sceneBanner.duration(bannerHeight * 1.5);
      sceneProduct.duration(productHeight * 1.5);
      sceneVideo.duration(videoHeight * 1.5);
      sceneVideo.offset(-winHeight/3);
   });

   var isCheckOffset = false;

   $(window).scroll(function() {
      if (!isCheckOffset) {
         isCheckOffset = true;
         productOffset = $('#product').offset().top - winHeight/2;
         videoOffset = $('#video').offset().top - winHeight/2;
      }
      scroll = $(window).scrollTop();
      checkActiveSection();
      if (scroll <= productOffset) {
         $('#layer1').css({
            'transform': `translate(0, ${scroll / 5}px)`
         });
         $('#layer2').css({
            'transform': `translate(0, ${scroll / 4.5}px)`
         });
         $('#layer3').css({
            'transform': `translate(0, ${scroll / 3.5}px)`
         });
         $('#layer4').css({
            'transform': `translate(0, ${scroll / 5.5}px)`
         });
         $('#layer5').css({
            'transform': `translate(0, ${scroll / 7.5}px)`
         });
         $('#layer6').css({
            'transform': `translate(0, ${scroll / 10}px)`
         });
      }
   });

   $('.product-list .item').on('click', function() {
      var scrollPos = $('#product').offset().top;
      var index = $(this).data('index');
      $('#list_tab').hide();
      $('#detail_tab').fadeIn(500);
      if (enableCatSlide) {
         enableCatSlide = false;
         $('#cat_list').slick({
            fade: true,
            prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
         });
         $('#cat_list').slick('slickGoTo', index);
      }
      else {
         $('#cat_list').slick('slickGoTo', index);
      }
      if (winWidth < 768) {
         $('html, body').stop().animate({
            scrollTop: scrollPos - 60
         }, 600, 'swing');
      }
      else if (winWidth >= 1600) {
         $('html, body').stop().animate({
            scrollTop: scrollPos - 85
         }, 900, 'swing');
      }
      else {
         $('html, body').stop().animate({
            scrollTop: scrollPos - 45
         }, 900, 'swing');
      }
   });

   $('.cat-item .nav-tab a').on('click', function() {
      if (!$(this).hasClass('active')) {
         var scrollPos = $('#product').offset().top;
         var href = $(this).data('href');
         var item = $(this).parents('.cat-item').find('.detail-item');
         $(this).parents('.nav-tab').find('a').removeClass('active');
         $(this).addClass('active');
         $(item).hide();
         $(href).show();
         if (winWidth < 768) {
            $('html, body').stop().animate({
               scrollTop: scrollPos - 60
            }, 600, 'swing');
         }
         else if (winWidth >= 1600) {
            $('html, body').stop().animate({
               scrollTop: scrollPos - 85
            }, 600, 'swing');
         }
         else {
            $('html, body').stop().animate({
               scrollTop: scrollPos - 45
            }, 600, 'swing');
         }
      }
   });
   
   $('#video').on('click', '.vid-item', function() {
      $('#modal_vid .vid-wrap').hide();
      $('body').addClass('ovf-hidden');
      var href = $(this).data('url');
      var vidUrl = 'https://www.youtube.com/embed/' + href + '?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;playsinline=1&amp;autoplay=1&amp;lopp=1&amp;iv_load_policy=3';
      $('#vid_detail').attr('src', vidUrl);
      $('#modal_vid .vid-wrap').show();
      $('#modal_vid').fadeIn(500);
   });
   
   $('#modal_vid .layer-mask, #modal_vid .close-btn').on('click', function() {
      $('#modal_vid').hide();
      $('body').removeClass('ovf-hidden');
      $('#vid_detail').attr('src', '');
   });
});