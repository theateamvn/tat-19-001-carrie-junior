$(function () {
   // init the controller
   var controller = new ScrollMagic.Controller();
   var dealerHeight = $('#dealer').height();
   var scroll = $(window).scrollTop();
   
   // Dealer
   var tlBubbleDealer = new TimelineMax({ yoyo: true });
   tlBubbleDealer.add([
      TweenMax.to(".dealer-bubble .left-1", 3, {
         yPercent: -380,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .left-2", 3, {
         yPercent: -580,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .left-3", 3, {
         yPercent: -750,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .left-4", 3, {
         yPercent: -800,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .left-5", 3, {
         yPercent: -320,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .left-6", 3, {
         yPercent: -180,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .right-1", 3, {
         yPercent: -80,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .right-2", 3, {
         yPercent: -720,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .right-3", 3, {
         yPercent: -980,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .right-4", 3, {
         yPercent: -980,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .right-5", 3, {
         yPercent: -480,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .right-6", 3, {
         yPercent: -340,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .right-7", 3, {
         yPercent: -400,
         ease: Power2.easeOut
      })
   ]);
    
   // Start animation when scrolling
   var sceneDealer = new ScrollMagic.Scene({
      triggerElement: "#dealer",
      duration: dealerHeight*2/3,
      triggerHook: "onLeave"
   })
   .setTween(tlBubbleDealer)
   .addTo(controller);
   
   $(window).on('resize', function() {
      dealerHeight = $('#dealer').height();
      sceneDealer.duration(dealerHeight);
   });
});