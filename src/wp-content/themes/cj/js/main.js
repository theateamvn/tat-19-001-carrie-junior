$(window).on('beforeunload', function() {
   $(window).scrollTop(0);
});

$(function () {
   var tlAnm = new TimelineMax();
   tlAnm.to('body', .7, {
      alpha: 1
   }, .2)
   .from('.nav-control', .5, {
      opacity: 0
   }, '-=.1')
   .staggerFrom('.main-nav li', .3, {
      x: 15,
      alpha: 0
   }, .12, '-=.5');

   $('#header .nav-control').on('click', function() {
      $('body').toggleClass('nav-active');
   });

   $('#scrollTop').on('click', function() {
      $('html, body').stop().animate({scrollTop : 0}, 800);
      return false;
   });

   var winHeight = window.innerHeight;
   var scroll = $(window).scrollTop();

   function onScrollUpdate() {
      if (scroll >= winHeight/2) $('#scrollTop').fadeIn(300);
      else $('#scrollTop').fadeOut(300);
   }
   
   $(window).on('resize', function() {
      winHeight = window.innerHeight;
      scroll = $(window).scrollTop();
      onScrollUpdate();
   });

   //Scroll Animation
   $(document).ready(function(){
      var controller = new ScrollMagic.Controller();
      var ourScene = new ScrollMagic.Scene({
         triggerElement: '.fade-in-down',
         offset: 50,
			triggerHook: 0.9,
         reverse: false
      })
         .setClassToggle('.fade-in-down','show')
         .addTo(controller);
   });
   $(document).ready(function(){
      var controller2 = new ScrollMagic.Controller();
      var ourScene2 = new ScrollMagic.Scene({
         triggerElement: '.fade-in-up',
         offset: 50,
			triggerHook: 0.9,
         reverse: false
      })
         .setClassToggle('.fade-in-up','show')
         .addTo(controller2);
   });
   $(document).ready(function(){
      var controller3 = new ScrollMagic.Controller();
      var ourScene3 = new ScrollMagic.Scene({
         triggerElement: '.fade-in-left',
         offset: 350,
			triggerHook: 0.9,
         reverse: false
      })
         .setClassToggle('.fade-in-left','show')
         .addTo(controller3);
   });
   $(document).ready(function(){
      var controller4 = new ScrollMagic.Controller();
      var ourScene4 = new ScrollMagic.Scene({
         triggerElement: '.fade-in-right',
         offset: 350,
			triggerHook: 0.9,
         reverse: false
      })
         .setClassToggle('.fade-in-right','show')
         .addTo(controller4);
   });
   $(document).ready(function(){
      var controller5 = new ScrollMagic.Controller();
      var ourScene5 = new ScrollMagic.Scene({
         triggerElement: '.blur-in',
         offset: 350,
			triggerHook: 0.9,
         reverse: false
      })
         .setClassToggle('.blur-in','show')
         .addTo(controller5);
   });
   $(document).ready(function(){
      var controller6 = new ScrollMagic.Controller();
      var ourScene6 = new ScrollMagic.Scene({
         triggerElement: '.spin-360',
         offset: 350,
			triggerHook: 0.9,
         reverse: false
      })
         .setClassToggle('.spin-360','show')
         .addTo(controller6);
   });
   $(document).ready(function(){
      var controller7 = new ScrollMagic.Controller();
      var ourScene7 = new ScrollMagic.Scene({
         triggerElement: '.txt-decoration',
         offset: 350,
			triggerHook: 0.9,
         reverse: false
      })
         .setClassToggle('.txt-decoration','show')
         .addTo(controller7);
   });
   $(document).ready(function(){
      var controller8 = new ScrollMagic.Controller();
      var ourScene8 = new ScrollMagic.Scene({
         triggerElement: '.vid-scale',
         offset: 50,
			triggerHook: 0.9,
         reverse: false
      })
         .setClassToggle('.vid-scale','show')
         .addTo(controller8);
   });
   //END Scroll Animation
});