$(function () {
   $('.promotion-list').slick({
      autoplay: true,
      dots: false,
      arrows: false,
      pauseOnHover: false,
      prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
      nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
   });
});