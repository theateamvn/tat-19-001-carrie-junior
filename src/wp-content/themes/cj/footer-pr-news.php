
   <footer id="footer">
      <a class="ft-logo" href="javascript:;">
         <img src="<?php bloginfo('template_directory');?>/images/footer-logo.png" alt="">
      </a>
      <p class="copyright">
         <span class="hidden-xs">
            © Copyright 2019 CARRIE JUNIOR. All rights reserved
         </span>
         <span class="hidden-sm hidden-md hidden-lg">
            © Copyright 2019 CARRIE JUNIOR.<br>
            All rights reserved
         </span>
         <div class="socials">
            <a href="<?php the_field('link_facebook', 'option'); ?>" target="_blank">
               <i class="fa fa-facebook" aria-hidden="true"></i>
            </a>
            <a href="<?php the_field('link_youtube', 'option'); ?>" target="_blank">
               <i class="fa fa-youtube-play" aria-hidden="true"></i>
            </a>
         </div>
      </p>
   </footer>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/bootstrap.js"></script>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/TweenMax.min.js"></script>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/ScrollMagic.min.js"></script>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/animation.gsap.min.js"></script>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/slick.js"></script>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/main.js"></script>

   <?php wp_footer();?>
</body>
</html>